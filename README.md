# datadog-homework
> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started
1. Install package manager on Mac
     ```
     /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
     ```
2. Install Node.js version manager (nodebrew)
    ```
    brew install nodebrew
    ```
3. Install Node.js
    ```
    nodebrew install v16.13.2 && nodebrew use v16.13.2
    ```

## How to run
- To start application:
  ```
  npm install && npm build && npm start
  ```
- Homepage: [http://localhost:3000](http://localhost:3000)
- To lint:
    ```
    npm run lint
    ```
- To run tests:
    ```
    npm run test
    ```

## Resource
- [Create React App guide](https://create-react-app.dev/docs/getting-started)
- [Axios guide](https://axios-http.com/docs/api_intro)
- [React guide](https://reactjs.org/docs/getting-started.html)
- [React Bootstrap guide](https://react-bootstrap.github.io/components/buttons/)

## TODOs
- [ ] Add [unit test](https://www.albertgao.xyz/2017/05/24/how-to-test-expressjs-with-jest-and-supertest/) & [React test](https://jestjs.io/docs/tutorial-react)
- [ ] Considering switching to TypeScript or Flow
