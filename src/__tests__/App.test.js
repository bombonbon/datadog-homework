import React from 'react';
import {
  render, screen, test, expect,
} from '@testing-library/react';
import App from '../App';

test('renders table', () => {
  render(<App />);
  expect(screen.getByText('Name')).toBeInTheDocument();
});
