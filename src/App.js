import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';

export default function App() {
  return (
    <Container>
      <Row>
        <Table>
          <thead>
            <tr>
              <thead>Name</thead>
              <thead>Owner</thead>
              <thead>Stars</thead>
              <thead>Link</thead>
              <thead>Details</thead>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>spark</td>
              <td>apache</td>
              <td>26,000</td>
              <td><a target="blank" href="https://github.com/apache/spark">apache/spark</a></td>
              <td><a target="blank" href="https://www.google.com">Details</a></td>
            </tr>
          </tbody>
        </Table>
      </Row>
    </Container>
  );
}
